package com.android.gvr.verizon.model;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.media.MediaMetadataRetriever;
import android.text.TextUtils;
import android.util.Log;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Modal class to hold Video data.
 * <p/>
 * Created by ankush.thakur on 6/22/2016.
 */
public class VideoContents implements Serializable {

    /**
     * TAG.
     */
    private static final String TAG = VideoContents.class.getSimpleName();

    private ArrayList<VideoContents> videos;

    /**
     * Hold boolean whether Video is static or not.
     */
    private boolean isStatic;

    /**
     * Holds the Video name.
     */
    private String name;

    /**
     * Hold boolean whether Video is static or not.
     */
    private String url;

    /**
     * Hold the Video thumbnail.
     */
    private String thumbnail;

    /**
     * Holds the Video duration.
     */
    private String mVideoDuration;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isStaticVideo() {
        return isStatic;
    }

    public void setIsStaticVideo(boolean isStatic) {
        this.isStatic = isStatic;
    }

    public String getVideoName() {
        return name;
    }

    public void setVideoName(String videoName) {
        this.name = videoName;
    }

    public String getVideoDuration(Context context) {
        long duration = 0;

        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        if (isStatic) {
            AssetFileDescriptor afd = null;
            try {
                afd = context.getAssets().openFd(url);
                retriever.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                afd.close();
            } catch (IOException e) {
                e.printStackTrace();
                Log.e(TAG , "Exception while setting data source in media player in getVideoDuration() "+ e.getMessage());
            }
        }else{
            //TODO : To check for getting http live streaming video file duration.
//                retriever.setDataSource(context, Uri.parse(getUrl()));
        }

        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        if (TextUtils.isEmpty(time)){
            time = "0";
        }
        duration = Long.parseLong( time );
        duration /= 1000;

        String durationText = String.format("%02d:%02d:%02d", duration / 3600,
                (duration % 3600) / 60, duration % 60);
        Log.d(TAG , "duration: "+ durationText);
        return durationText;
    }

    public void setVideoDuration(String videoDuration) {
        this.mVideoDuration = videoDuration;
    }

    public Drawable getVideoImage(Context context) {
        Resources resources = context.getResources();
        // get resource id by image name
        int drawableId = resources.getIdentifier(thumbnail, "drawable", context.getPackageName());
        return resources.getDrawable(drawableId);
    }

    public void setVideoImage(String videoImage) {
        this.thumbnail = videoImage;
    }

    public ArrayList<VideoContents> getVideosContentList() {
        return videos;
    }

    public void setVideos(ArrayList<VideoContents> videos) {
        this.videos = videos;
    }
}
