package com.android.gvr.verizon.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * This is NetworkUpdateReceiver to listen to the network change(on/off) state
 * and handles the scenarios accordingly.
 */
public class NetworkUpdateReceiver extends BroadcastReceiver {

    private INetworkSwitchListener mNetworkSwitchListener;

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        if (action.equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
            if (NetworkUtil.isNetworkAvailable(context)) {
                mNetworkSwitchListener.onNetworkConnected();
            } else {
                mNetworkSwitchListener.onNetworkDisconnected();
            }
        }
    }

    public void setNetworkSwitchListener(INetworkSwitchListener networkSwitchListener) {
        mNetworkSwitchListener = networkSwitchListener;
    }
}
