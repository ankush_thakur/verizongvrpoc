package com.android.gvr.verizon.video;

import android.media.MediaPlayer;

import com.android.gvr.verizon.video.factory.GVRMainFactory;

import org.gearvrf.GVRContext;
import org.gearvrf.GVRMain;
import org.gearvrf.GVRScene;

/**
 * This is GVRMain class. Any activity should use this GVRMain to render the UI elements.
 */
public abstract class VideoPlayScreenRendererScript extends GVRMain {

    /**
     * Holds GVRContext.
     */
    protected GVRContext mGVRContext;

    /**
     * Holds Main scene object.
     */
    protected GVRScene mainScene;

    /**
     * Holds MediaPlayer object.
     */
    protected MediaPlayer mMediaPlayer;

    /**
     * Holds boolean for Video Play screen touch.
     */
    protected boolean mIsTouched = false;

    /**
     * Holds boolean for Video Play screen single tap.
     */
    protected boolean mIsSingleTapped = false;

    /**
     * Adds the screen.
     */
    public abstract void addScreen();

    /**
     * Adds cursor to the screen.
     */
    public abstract void addCursor();

    /**
     * Adds MediaController Buttons to the video.
     */
    public abstract void addMediaControllerButtons();

    /**
     * Adds seekbar.
     */
    public abstract void addSeekBar();

    /**
     * Plays video content.
     */
    public abstract void playVideo();

    @Override
    public void onInit(GVRContext gvrContext) throws Throwable {
        mGVRContext = gvrContext;
        mainScene = gvrContext.getNextMainScene();
        mMediaPlayer = new MediaPlayer();

        VideoPlayScreenRendererScript videoGVRMain = GVRMainFactory.getGVRMain();
        videoGVRMain.addCursor();
        videoGVRMain.addScreen();
        videoGVRMain.addSeekBar();
        videoGVRMain.addMediaControllerButtons();
        videoGVRMain.playVideo();

    }

    public void onPause() {
        if (mMediaPlayer != null) {
            mMediaPlayer.pause();
        }
    }

    public void onTouchEvent() {
        mIsTouched = true;
    }

    public void onSingleTap() {
        mIsSingleTapped = true;
    }

    public void cleanUpResources() {
        if (mMediaPlayer != null) {
            mMediaPlayer.stop();
            mMediaPlayer.release();
        }
    }

    @Override
    public abstract void onStep();
}
