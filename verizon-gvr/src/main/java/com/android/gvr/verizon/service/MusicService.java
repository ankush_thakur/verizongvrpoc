package com.android.gvr.verizon.service;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;

import com.android.gvr.verizon.R;

/**
 * Background service to play music when user is on video list screen only.
 */
public class MusicService extends Service {

    /**
     * Holds the MediaPlayer object.
     */
    private MediaPlayer mMediaPlayer;

    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mMediaPlayer = MediaPlayer.create(this, R.raw.music);
        mMediaPlayer.setLooping(true); // Set looping
        mMediaPlayer.setVolume(50, 50);
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        mMediaPlayer.start();
        return 1;
    }

    @Override
    public void onDestroy() {
        mMediaPlayer.stop();
        mMediaPlayer.release();
    }
}