package com.android.gvr.verizon.util;

public interface INetworkSwitchListener {
    void onNetworkDisconnected();
    void onNetworkConnected();
}
