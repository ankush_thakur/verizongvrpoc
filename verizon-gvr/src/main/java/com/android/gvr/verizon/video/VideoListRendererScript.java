/* Copyright 2015 Samsung Electronics Co., LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.gvr.verizon.video;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.InputDevice;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.MotionEvent.PointerCoords;
import android.view.MotionEvent.PointerProperties;

import com.android.gvr.verizon.R;
import com.android.gvr.verizon.activity.VideoListActivity;

import org.gearvrf.FutureWrapper;
import org.gearvrf.GVRAndroidResource;
import org.gearvrf.GVRBaseSensor;
import org.gearvrf.GVRContext;
import org.gearvrf.GVRCursorController;
import org.gearvrf.GVRMain;
import org.gearvrf.GVRMesh;
import org.gearvrf.GVRScene;
import org.gearvrf.GVRSceneObject;
import org.gearvrf.GVRTexture;
import org.gearvrf.ISensorEvents;
import org.gearvrf.SensorEvent;
import org.gearvrf.io.CursorControllerListener;
import org.gearvrf.io.GVRControllerType;
import org.gearvrf.io.GVRInputManager;
import org.gearvrf.scene_objects.GVRSphereSceneObject;
import org.gearvrf.scene_objects.GVRViewSceneObject;
import org.gearvrf.scene_objects.view.GVRFrameLayout;

import java.util.List;
import java.util.concurrent.Future;

/**
 * GVRMain class to hold the frame layout and show it on to the screen.
 *
 * @author ankush.thakur
 */
public class VideoListRendererScript extends GVRMain {
    private static final String TAG = VideoListRendererScript.class.getSimpleName();
    private static final int KEY_EVENT = 1;
    private static final int MOTION_EVENT = 2;

    /**
     * Holds scene object for video list layout to be added to the main scene.
     */
    private GVRViewSceneObject mLayoutSceneObject;

    /**
     * Holds context.
     */
    private GVRContext mContext;

    /**
     * Constants used for scene object display.
     */
    private static final float QUAD_X = 1.0f;
    private static final float QUAD_Y = 1.0f;
    private static final float HALF_QUAD_X = QUAD_X / 2.0f;
    private static final float HALF_QUAD_Y = QUAD_Y / 2.0f;
    private static final float DEPTH = -1.5f;

    /**
     * Holds layout for video list.
     */
    private final GVRFrameLayout mFrameLayout;

    /**
     * Holds video list layout width.
     */
    private int mFrameWidth;

    /**
     * Holds video list layout height.
     */
    private int mFrameHeight;

    /**
     * Holds scene object, into which all the scene object will be added.
     */
    private GVRScene mMainScene;

    /**
     * Holds handler object used for dispatching video list layout touch..
     */
    private Handler mainThreadHandler;

    private final static PointerProperties[] pointerProperties;
    private final static PointerCoords[] pointerCoordsArray;
    private final static PointerCoords pointerCoords;

    /**
     * Holds GVRSceneObject for cursor.
     */
    private GVRSceneObject mCursor;

    static {
        PointerProperties properties = new PointerProperties();
        properties.id = 0;
        properties.toolType = MotionEvent.TOOL_TYPE_MOUSE;
        pointerProperties = new PointerProperties[]{properties};
        pointerCoords = new PointerCoords();
        pointerCoordsArray = new PointerCoords[]{pointerCoords};
    }

    /**
     * Constructor.
     *
     * @param activity    Holds the reference of activity
     * @param frameLayout Holds the frame layout
     */
    public VideoListRendererScript(VideoListActivity activity,
                                   final GVRFrameLayout frameLayout) {
        this.mFrameLayout = frameLayout;
        mainThreadHandler = new Handler(activity.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                if (msg != null && msg.what == MOTION_EVENT) {
                    try {
                        MotionEvent motionEvent = (MotionEvent) msg.obj;
                        frameLayout.dispatchTouchEvent(motionEvent);
                        frameLayout.invalidate();
                        motionEvent.recycle();
                    } catch (IllegalArgumentException e) {
                        Log.e(TAG, "IllegalArgumentException: "+e.getMessage());
                    }
                }
            }
        };
    }

    @Override
    public void onInit(final GVRContext gvrContext) throws Throwable {
        mContext = gvrContext;

        mLayoutSceneObject = new GVRViewSceneObject(gvrContext, mFrameLayout,
                mContext.createQuad(QUAD_X, QUAD_Y));
        mMainScene = gvrContext.getNextMainScene();

        GVRSphereSceneObject sphereObject = null;
        Future<GVRTexture> texture = gvrContext.loadFutureTexture(new GVRAndroidResource(gvrContext, R.raw.photosphere));
        sphereObject = new GVRSphereSceneObject(gvrContext, false, texture);
        mMainScene.addSceneObject(sphereObject);

        mLayoutSceneObject.getTransform().setPosition(0.0f, 0.0f, DEPTH);
        mMainScene.addSceneObject(mLayoutSceneObject);
        mLayoutSceneObject.getRenderData().setDepthTest(false);
        mLayoutSceneObject.getRenderData().setRenderingOrder(100000);

        mFrameWidth = mFrameLayout.getWidth();
        mFrameHeight = mFrameLayout.getHeight();

        // set up the input manager for the main scene
        GVRInputManager inputManager = gvrContext.getInputManager();
        inputManager.addCursorControllerListener(listener);
        for (GVRCursorController cursor : inputManager.getCursorControllers()) {
            listener.onCursorControllerAdded(cursor);
        }
        GVRBaseSensor sensor = new GVRBaseSensor(gvrContext);
        sphereObject.getEventReceiver().addListener(eventListener);
        sphereObject.setSensor(sensor);
    }

    private ISensorEvents eventListener = new ISensorEvents() {
        private static final float SCALE = 10.0f;
        private float savedMotionEventX, savedMotionEventY, savedHitPointX,
                savedHitPointY;

        @Override
        public void onSensorEvent(SensorEvent event) {
            List<MotionEvent> motionEvents = event.getCursorController().getMotionEvents();

            for (MotionEvent motionEvent : motionEvents) {
                if (motionEvent.getAction() == MotionEvent.ACTION_MOVE) {
                    pointerCoords.x = savedHitPointX
                            + ((motionEvent.getX() - savedMotionEventX) * SCALE);
                    pointerCoords.y = savedHitPointY
                            + ((motionEvent.getY() - savedMotionEventY) * SCALE);
                } else {
                    float[] hitPoint = event.getHitPoint();
                    pointerCoords.x = ((hitPoint[0] + HALF_QUAD_X) / QUAD_X) * mFrameWidth;
                    pointerCoords.y = (-(hitPoint[1] - HALF_QUAD_Y) / QUAD_Y) * mFrameHeight;

                    if (motionEvent.getAction() == KeyEvent.ACTION_DOWN) {
                        // save the co ordinates on down
                        savedMotionEventX = motionEvent.getX();
                        savedMotionEventY = motionEvent.getY();

                        savedHitPointX = pointerCoords.x;
                        savedHitPointY = pointerCoords.y;
                    }
                }

                final MotionEvent clone = MotionEvent.obtain(
                        motionEvent.getDownTime(), motionEvent.getEventTime(),
                        motionEvent.getAction(), 1, pointerProperties,
                        pointerCoordsArray, 0, 0, 1f, 1f, 0, 0,
                        InputDevice.SOURCE_TOUCHSCREEN, 0);

                Message message = Message.obtain(mainThreadHandler, MOTION_EVENT, 0, 0,
                        clone);
                mainThreadHandler.sendMessage(message);
            }

            List<KeyEvent> keyEvents = event.getCursorController().getKeyEvents();
            for (KeyEvent keyEvent : keyEvents) {
                Message message = Message.obtain(mainThreadHandler, KEY_EVENT,
                        keyEvent.getKeyCode(), 0, null);
                mainThreadHandler.sendMessage(message);
            }
        }
    };

    private CursorControllerListener listener = new CursorControllerListener() {

        @Override
        public void onCursorControllerRemoved(GVRCursorController controller) {
            if (controller.getControllerType() == GVRControllerType.GAZE) {
                if (mCursor != null) {
                    mMainScene.getMainCameraRig().removeChildObject(mCursor);
                }
                controller.setEnable(false);
            }
        }

        @Override
        public void onCursorControllerAdded(GVRCursorController controller) {
            // Only allow only gaze
            if (controller.getControllerType() == GVRControllerType.GAZE) {
                mCursor = new GVRSceneObject(mContext,
                        new FutureWrapper<GVRMesh>(mContext.createQuad(0.1f, 0.1f)),
                        mContext.loadFutureTexture(new GVRAndroidResource(mContext, R.raw.cursor)));
                mCursor.getTransform().setPosition(0.0f, 0.0f, DEPTH);
                mMainScene.getMainCameraRig().addChildObject(mCursor);
                mCursor.getRenderData().setDepthTest(false);
                mCursor.getRenderData().setRenderingOrder(100000);
                controller.setPosition(0.0f, 0.0f, DEPTH);
                controller.setNearDepth(DEPTH);
                controller.setFarDepth(DEPTH);
            } else {
                // disable all other types
                controller.setEnable(false);
            }
        }
    };

    @Override
    public void onStep() {
        // unused
    }
}
