package com.android.gvr.verizon.video.factory;

import com.android.gvr.verizon.video.Video360RendererScript;
import com.android.gvr.verizon.video.VideoLiveStreamingRendererScript;
import com.android.gvr.verizon.video.VideoPlayScreenRendererScript;

import org.gearvrf.GVRActivity;

/**
 * Factory class, returns the respective GVRMain object as per the video type(static or live streaming).
 * <li>It contains overloaded method getGVRMain(...) and getGVRMain().
 * <li>When one needs to create new object then getGVRMain(...) should be called and when one needs to get the current GVRMain object then getGVRMain() should be called.
 */
public class GVRMainFactory {

    /**
     * Holds VideoGVRMain as per the video type(static/live streaming).
     */
    private static VideoPlayScreenRendererScript sGvrMain;

    /**
     * @param context Context
     * @param isStaticVideo whether video is static or live streaming
     * @param videoUrl file name for static video / url for live streaming video
     * @return GVRMain object either for Video360RendererScript(for static videos) or VideoLiveStreamingRendererScript(for live streaming videos)
     */
    public static VideoPlayScreenRendererScript getGVRMain(GVRActivity context, boolean isStaticVideo, String videoUrl){

        if (isStaticVideo) {
            sGvrMain = new Video360RendererScript(context, videoUrl);
        }else{
            sGvrMain = new VideoLiveStreamingRendererScript(context, videoUrl);
        }
        return sGvrMain;
    }

    /**
     * @return Video360RendererScript(for static videos) or VideoLiveStreamingRendererScript(for live streaming videos) object.
     */
    public static VideoPlayScreenRendererScript getGVRMain(){
        return sGvrMain;
    }


}
