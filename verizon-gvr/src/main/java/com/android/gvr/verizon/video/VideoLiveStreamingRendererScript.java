/* Copyright 2015 Samsung Electronics Co., LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.gvr.verizon.video;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.android.gvr.verizon.R;
import com.android.gvr.verizon.activity.VideoListActivity;
import com.android.gvr.verizon.util.NetworkUtil;
import com.android.gvr.verizon.view.MediaControllerButton;
import com.android.gvr.verizon.view.livestreamingvideo.AdditiveShader;
import com.android.gvr.verizon.view.livestreamingvideo.RadiosityShader;
import com.android.gvr.verizon.view.livestreamingvideo.ScreenShader;
import com.android.gvr.verizon.view.livestreamingvideo.Seekbar;

import org.gearvrf.GVRActivity;
import org.gearvrf.GVRAndroidResource;
import org.gearvrf.GVRExternalTexture;
import org.gearvrf.GVREyePointeeHolder;
import org.gearvrf.GVRMaterial;
import org.gearvrf.GVRMesh;
import org.gearvrf.GVRPicker;
import org.gearvrf.GVRRenderData;
import org.gearvrf.GVRRenderData.GVRRenderMaskBit;
import org.gearvrf.GVRRenderData.GVRRenderingOrder;
import org.gearvrf.GVRRenderPass;
import org.gearvrf.GVRSceneObject;
import org.gearvrf.GVRTexture;
import org.gearvrf.scene_objects.GVRVideoSceneObject;
import org.gearvrf.scene_objects.GVRViewSceneObject;
import org.gearvrf.scene_objects.view.GVRFrameLayout;

import java.io.IOException;

/**
 * This class is used to render the Video screen for live streaming videos.
 */
public class VideoLiveStreamingRendererScript extends VideoPlayScreenRendererScript {

    /**
     * TAG.
     */
    private static final String TAG = VideoLiveStreamingRendererScript.class.getSimpleName();

    /**
     * Holds number of theater screen.
     */
    private static final int NUMBER_OF_THEATER_SCREENS = 2;

    /**
     * Holds theaterScreen scene object.
     */
    private GVRSceneObject[] mTheater = new GVRSceneObject[NUMBER_OF_THEATER_SCREENS];

    private GVRSceneObject mSceneObject = null;
    private GVRSceneObject mScreen = null;

    private GVRSceneObject mOculusSceneObject1 = null;
    private GVRSceneObject mOculusSceneObject2 = null;
    private GVRSceneObject mOculusScreen = null;

    /**
     * Hold MediaController scene objects.
     */
    private GVRSceneObject mHeadTracker = null;
    private MediaControllerButton mPlayButton = null;
    private MediaControllerButton mPauseButton = null;
    private MediaControllerButton mForwardButton = null;
    private MediaControllerButton mBackwardButton = null;
    private MediaControllerButton mImaxButton = null;
    private MediaControllerButton mSelectButton = null;
    private MediaControllerButton mExitButton = null;
    private GVRSceneObject mButtonBoard = null;
    private Seekbar mSeekbar = null;

    private boolean mIsUIHidden = true;
    private boolean mIsIMAX = false;

    private float mTransitionWeight = 0.0f;
    private float mTransitionTarget = 0.0f;

    /**
     * Holds current selected theater.
     */
    private int mCurrentTheater = 0;

    /**
     * Constants for theater screen fade.
     */
    private float mFadeWeight = 0.0f;
    private float mFadeTarget = 1.0f;

    /**
     * Holds activity context.
     */
    private GVRActivity mActivity;

    /**
     * Holds video url.
     */
    private String mVideoUrl;

    /**
     * Holds Network dialog scene object.
     */
    private GVRSceneObject mNetworkLayoutSceneObject;

    public VideoLiveStreamingRendererScript(GVRActivity activity, String videoUrl) {
        mActivity = activity;
        mVideoUrl = videoUrl;
    }

    @Override
    public void addScreen() {

        RadiosityShader radiosityShader = new RadiosityShader(mGVRContext);
        AdditiveShader additiveShader = new AdditiveShader(mGVRContext);
        ScreenShader screenShader = new ScreenShader(mGVRContext);
        GVRExternalTexture screenTexture = new GVRExternalTexture(mGVRContext);
        mTheater[0] = new GVRSceneObject(mGVRContext);

        /**
         * FXGear Background
         */
        GVRMesh backgroundMesh = null;
        try {
            backgroundMesh = mGVRContext
                    .loadMesh(new GVRAndroidResource(mGVRContext,
                            "theater1/theater_background.obj"));
            GVRTexture leftBackgroundLightOffTexture = mGVRContext
                    .loadTexture(new GVRAndroidResource(mGVRContext,
                            "theater1/theater_background_left_light_off.jpg"));
            GVRTexture leftBackgroundLightOnTexture = mGVRContext
                    .loadTexture(new GVRAndroidResource(mGVRContext,
                            "theater1/theater_background_left_light_on.jpg"));
            mSceneObject = new GVRSceneObject(mGVRContext, backgroundMesh,
                    leftBackgroundLightOffTexture);
            mSceneObject.getTransform().setPosition(-0.031f, 0.0f, 0.0f);
            mSceneObject.getRenderData().setCullFace(GVRRenderPass.GVRCullFaceEnum.None);

            mTheater[0].addChildObject(mSceneObject);

            /*
             * Radiosity settings
             */
            mSceneObject.getRenderData().getMaterial()
                    .setShaderType(radiosityShader.getShaderId());
            mSceneObject.getRenderData().getMaterial()
                    .setTexture(RadiosityShader.TEXTURE_OFF_KEY,
                            leftBackgroundLightOffTexture);
            mSceneObject.getRenderData().getMaterial()
                    .setTexture(RadiosityShader.TEXTURE_ON_KEY,
                            leftBackgroundLightOnTexture);
            mSceneObject.getRenderData().getMaterial()
                    .setTexture(RadiosityShader.SCREEN_KEY, screenTexture);

            /*
             * Uv setting for radiosity
             */

            GVRMesh radiosity_mesh = mGVRContext
                    .loadMesh(new GVRAndroidResource(mGVRContext,
                            "theater1/radiosity.obj"));
            backgroundMesh.setNormals(radiosity_mesh.getVertices());

            /*
             * Screen
             */

            GVRMesh screenMesh = mGVRContext.loadMesh(new GVRAndroidResource(
                    mGVRContext, "theater1/screen.obj"));
            GVRRenderData renderData = new GVRRenderData(mGVRContext);
            GVRMaterial material = new GVRMaterial(mGVRContext,
                    screenShader.getShaderId());
            material.setTexture(ScreenShader.SCREEN_KEY, screenTexture);
            renderData.setMesh(screenMesh);
            renderData.setMaterial(material);

            mScreen = new GVRVideoSceneObject(mGVRContext, screenMesh, mMediaPlayer,
                    screenTexture, GVRVideoSceneObject.GVRVideoType.MONO);
            mScreen.attachRenderData(renderData);
            mScreen.getRenderData().setCullFace(GVRRenderPass.GVRCullFaceEnum.None);

            mTheater[0].addChildObject(mScreen);

            mainScene.addSceneObject(mTheater[0]);

            /*
             * Oculus Background
             */
            mTheater[1] = new GVRSceneObject(mGVRContext);

            GVRMesh backgroundMesh1 = mGVRContext
                    .loadMesh(new GVRAndroidResource(mGVRContext,
                            "theater2/cinema.obj"));
            GVRMesh backgroundMesh2 = mGVRContext
                    .loadMesh(new GVRAndroidResource(mGVRContext,
                            "theater2/additive.obj"));
            GVRTexture AdditiveTexture = mGVRContext
                    .loadTexture(new GVRAndroidResource(mGVRContext,
                            "theater2/additive.png"));
            GVRTexture BackgroundLightOffTexture = mGVRContext
                    .loadTexture(new GVRAndroidResource(mGVRContext,
                            "theater2/cinema1.png"));
            GVRTexture BackgroundLightOnTexture = mGVRContext
                    .loadTexture(new GVRAndroidResource(mGVRContext,
                            "theater2/cinema2.png"));
            mOculusSceneObject1 = new GVRSceneObject(mGVRContext,
                    backgroundMesh1, BackgroundLightOnTexture);
            mOculusSceneObject1.getRenderData().setCullFace(GVRRenderPass.GVRCullFaceEnum.None);
            mOculusSceneObject2 = new GVRSceneObject(mGVRContext,
                    backgroundMesh2, AdditiveTexture);
            mOculusSceneObject2.getRenderData().setCullFace(GVRRenderPass.GVRCullFaceEnum.None);
            mOculusSceneObject2.getRenderData().setRenderingOrder(2500);

            mTheater[1].addChildObject(mOculusSceneObject1);
            mTheater[1].addChildObject(mOculusSceneObject2);

            /*
             * Radiosity settings
             */
            mOculusSceneObject1.getRenderData().getMaterial()
                    .setShaderType(radiosityShader.getShaderId());
            mOculusSceneObject1
                    .getRenderData()
                    .getMaterial()
                    .setTexture(RadiosityShader.TEXTURE_OFF_KEY,
                            BackgroundLightOnTexture);
            mOculusSceneObject1
                    .getRenderData()
                    .getMaterial()
                    .setTexture(RadiosityShader.TEXTURE_ON_KEY,
                            BackgroundLightOffTexture);
            mOculusSceneObject1.getRenderData().getMaterial()
                    .setTexture(RadiosityShader.SCREEN_KEY, screenTexture);

            mOculusSceneObject2.getRenderData().getMaterial()
                    .setShaderType(additiveShader.getShaderId());
            mOculusSceneObject2.getRenderData().getMaterial()
                    .setTexture(AdditiveShader.TEXTURE_KEY, AdditiveTexture);

            /*
             * Uv setting for radiosity
             */

            GVRMesh oculus_radiosity_mesh1 = mGVRContext
                    .loadMesh(new GVRAndroidResource(mGVRContext,
                            "theater2/radiosity1.obj"));
            GVRMesh oculus_radiosity_mesh2 = mGVRContext
                    .loadMesh(new GVRAndroidResource(mGVRContext,
                            "theater2/radiosity2.obj"));
            backgroundMesh1.setNormals(oculus_radiosity_mesh1.getVertices());
            backgroundMesh2.setNormals(oculus_radiosity_mesh2.getVertices());

            /*
             * Screen
             */

            GVRMesh oculus_screenMesh = mGVRContext
                    .loadMesh(new GVRAndroidResource(mGVRContext,
                            "theater2/screen.obj"));
            GVRRenderData oculus_renderData = new GVRRenderData(mGVRContext);
            GVRMaterial oculus_material = new GVRMaterial(mGVRContext,
                    screenShader.getShaderId());
            oculus_material.setTexture(ScreenShader.SCREEN_KEY, screenTexture);
            oculus_renderData.setMesh(oculus_screenMesh);
            oculus_renderData.setMaterial(oculus_material);

            mOculusScreen = new GVRVideoSceneObject(mGVRContext, oculus_screenMesh, mMediaPlayer,
                    screenTexture, GVRVideoSceneObject.GVRVideoType.MONO);
            mOculusScreen.attachRenderData(oculus_renderData);
            mOculusScreen.getRenderData().setCullFace(GVRRenderPass.GVRCullFaceEnum.None);

            mTheater[1].addChildObject(mOculusScreen);

            float pivot_x = -3.353f;
            float pivot_y = 0.401f;
            float pivot_z = -0.000003f;

            mTheater[1].getTransform().setPosition(-pivot_x, -pivot_y, -pivot_z);
            mTheater[1].getTransform().rotateByAxisWithPivot(90.0f, 0.0f, 1.0f,
                    0.0f, 0.0f, 0.0f, 0.0f);

            mainScene.addSceneObject(mTheater[1]);
            for (int i = 0; i < mTheater[1].getChildrenCount(); i++)
                mTheater[1].getChildByIndex(i).getRenderData().setRenderMask(0);

            //TODO : Need to implement Network dialog display implementation.
            addNetworkDialogToScene();

        } catch (IOException e) {
            e.printStackTrace();
            mActivity.finish();
            Log.e(TAG, "Assets were not loaded in addScreen(). Stopping application!");
        }
    }

    private void addNetworkDialogToScene() {
        //TODO : Need to implement Network dialog display implementation.
        /*GVRFrameLayout gvrFrameLayout = new GVRFrameLayout(mGVRContext.getActivity());
        gvrFrameLayout.setAlpha(0);
        gvrFrameLayout.setBackgroundColor(Color.parseColor("#00000000"));
        View.inflate(mGVRContext.getContext(), R.layout.network_layout, gvrFrameLayout);

        mNetworkLayoutSceneObject = new GVRViewSceneObject(mGVRContext, gvrFrameLayout,
                mGVRContext.createQuad(1.0f, 1.0f));

        Button okButton = (Button) gvrFrameLayout.findViewById(R.id.ok_button_videolist_network_dialog);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                displayNetworkDialog(false);
                mainScene.removeSceneObject(mNetworkLayoutSceneObject);
            }
        });

        mNetworkLayoutSceneObject.getTransform().setPosition(0.5f, -0.2f, -1.5f);
        mainScene.addSceneObject(mNetworkLayoutSceneObject);
        mNetworkLayoutSceneObject.getRenderData().setDepthTest(false);
        mNetworkLayoutSceneObject.getRenderData().setRenderingOrder(100000);*/
    }

    @Override
    public void addCursor() {
        /**
         * Head tracker(Cursor)
         */
        GVRTexture headTrackerTexture = null;
        try {
            headTrackerTexture = mGVRContext
                    .loadTexture(new GVRAndroidResource(mGVRContext,
                            "head-tracker.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        mHeadTracker = new GVRSceneObject(mGVRContext,
                mGVRContext.createQuad(0.5f, 0.5f), headTrackerTexture);
        mHeadTracker.getTransform().setPositionZ(-9.0f);
        mHeadTracker.getRenderData().setRenderingOrder(
                GVRRenderingOrder.OVERLAY);
        mHeadTracker.getRenderData().setDepthTest(false);
        mHeadTracker.getRenderData().setRenderingOrder(100000);
        mainScene.getMainCameraRig().addChildObject(mHeadTracker);
    }

    @Override
    public void addMediaControllerButtons() {
        /**
         * Play button
         */
        GVRTexture mInactivePlay = null;
        try {
            mInactivePlay = mGVRContext.loadTexture(new GVRAndroidResource(
                    mGVRContext, "button/play-inactive.png"));

            GVRTexture mActivePlay = mGVRContext.loadTexture(new GVRAndroidResource(
                    mGVRContext, "button/play-active.png"));
            mPlayButton = new MediaControllerButton(mGVRContext, mGVRContext.createQuad(0.7f, 0.7f),
                    mActivePlay, mInactivePlay);
            mPlayButton.setPosition(-0.7f, -0.8f, -8.0f);
            mainScene.addSceneObject(mPlayButton);

            /*
             * Pause button
             */
            GVRTexture mInactivePause = mGVRContext.loadTexture(new GVRAndroidResource(
                    mGVRContext, "button/pause-inactive.png"));
            GVRTexture mActivePause = mGVRContext.loadTexture(new GVRAndroidResource(
                    mGVRContext, "button/pause-active.png"));
            mPauseButton = new MediaControllerButton(mGVRContext, mGVRContext.createQuad(0.7f, 0.7f),
                    mActivePause, mInactivePause);
            mPauseButton.setPosition(-0.7f, -0.8f, -8.0f);
            mainScene.addSceneObject(mPauseButton);

            /*
             * FORWARD button
             */
            GVRTexture mInactiveFront = mGVRContext.loadTexture(new GVRAndroidResource(
                    mGVRContext, "button/front-inactive.png"));
            GVRTexture mActiveFront = mGVRContext.loadTexture(new GVRAndroidResource(
                    mGVRContext, "button/front-active.png"));
            mForwardButton = new MediaControllerButton(mGVRContext, mGVRContext.createQuad(0.7f, 0.7f),
                    mActiveFront, mInactiveFront);
            mForwardButton.setPosition(0.2f, -0.8f, -8.0f);
            mainScene.addSceneObject(mForwardButton);

            /*
             * BACKWARD button
             */
            GVRTexture mInactiveBack = mGVRContext.loadTexture(new GVRAndroidResource(
                    mGVRContext, "button/back-inactive.png"));
            GVRTexture mActiveBack = mGVRContext.loadTexture(new GVRAndroidResource(
                    mGVRContext, "button/back-active.png"));
            mBackwardButton = new MediaControllerButton(mGVRContext, mGVRContext.createQuad(0.7f, 0.7f),
                    mActiveBack, mInactiveBack);
            mBackwardButton.setPosition(-1.6f, -0.8f, -8.0f);
            mainScene.addSceneObject(mBackwardButton);

            /*
             * Imax button
             */
            GVRTexture mInactiveImax = mGVRContext.loadTexture(new GVRAndroidResource(
                    mGVRContext, "button/imaxoutline.png"));
            GVRTexture mActiveImax = mGVRContext.loadTexture(new GVRAndroidResource(
                    mGVRContext, "button/imaxselect.png"));
            mImaxButton = new MediaControllerButton(mGVRContext, mGVRContext.createQuad(0.9f, 0.35f),
                    mActiveImax, mInactiveImax);
            mImaxButton.setPosition(1.2f, -0.9f, -7.5f);
            mainScene.addSceneObject(mImaxButton);

            /*
             * Select button
             */
            GVRTexture mInactiveSelect = mGVRContext.loadTexture(new GVRAndroidResource(
                    mGVRContext, "button/selectionselect.png"));
            GVRTexture mActiveSelect = mGVRContext.loadTexture(new GVRAndroidResource(
                    mGVRContext, "button/selectionoutline.png"));
            mSelectButton = new MediaControllerButton(mGVRContext, mGVRContext.createQuad(0.9f, 0.35f),
                    mActiveSelect, mInactiveSelect);
            mSelectButton.setPosition(-2.5f, -0.9f, -7.5f);
            mainScene.addSceneObject(mSelectButton);

            /*
             * Exit button
             */
            GVRTexture mInactiveExit = mGVRContext.loadTexture(new GVRAndroidResource(
                    mGVRContext, "button/exit_unpressed.png"));
            GVRTexture mActiveExit = mGVRContext.loadTexture(new GVRAndroidResource(
                    mGVRContext, "button/exit_pressed.png"));
            mExitButton = new MediaControllerButton(mGVRContext, mGVRContext.createQuad(0.9f, 0.35f),
                    mActiveExit, mInactiveExit);
            mExitButton.setPosition(2.2f, -0.9f, -7.5f);
            mainScene.addSceneObject(mExitButton);

            /*
             * Button board
             */
            mButtonBoard = new GVRSceneObject(mGVRContext,
                    mGVRContext.createQuad(8.2f, 1.35f),
                    mGVRContext.loadTexture(new GVRAndroidResource(mGVRContext,
                            "button/button-board.png")));
            mButtonBoard.getTransform().setPosition(-0.1f, -0.6f, -8.0f);
            mButtonBoard.getRenderData().setRenderingOrder(
                    GVRRenderingOrder.TRANSPARENT);
            mainScene.addSceneObject(mButtonBoard);

            showMediaControllers(false);

        } catch (IOException e) {
            e.printStackTrace();
            mActivity.finish();
            Log.e(TAG, "Assets were not loaded in addMediaControllerButtons() . Stopping application!");
        }
    }

    @Override
    public void addSeekBar() {
        mSeekbar = new Seekbar(mGVRContext);
        mainScene.addSceneObject(mSeekbar);
    }

    @Override
    public void playVideo() {
        mMediaPlayer.setLooping(true);

        try {
            mMediaPlayer.setDataSource(mGVRContext.getContext(), Uri.parse(mVideoUrl));
            mMediaPlayer.prepare();
            mMediaPlayer.start();
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "Exception while playing Live streaming video. Stopping application!");
            mActivity.finish();
        }

    }

    @Override
    public void onStep() {

        if (!NetworkUtil.isNetworkAvailable(mGVRContext.getContext())){
            displayNetworkDialog(true);
        }else{
            displayNetworkDialog(false);
        }

        setTheaterScreenScaling();

        boolean isTouched = mIsTouched;
        boolean isSingleTapped = mIsSingleTapped;
        mIsTouched = false;
        mIsSingleTapped = false;

        boolean isUIHiden = mIsUIHidden;
        boolean isAnythingPointed = false;

        if (!mIsUIHidden) {

            showMediaControllers(true);

            // Detect scene objects(MediaController buttons) here.
            GVREyePointeeHolder[] pickedHolders = GVRPicker.pickScene(mGVRContext.getMainScene());
            boolean playPauseButtonPointed = false;
            boolean forwardButtonPointed = false;
            boolean backwardButtonPointed = false;
            boolean imaxButtonPointed = false;
            boolean selectButtonPointed = false;
            boolean exitButtonPointed = false;
            boolean networkDialogPointed = false;
            Float seekbarRatio = mSeekbar.getRatio(mGVRContext.getMainScene()
                    .getMainCameraRig().getLookAt());

            for (GVREyePointeeHolder holder : pickedHolders) {
                if (holder.equals(mPlayButton.getEyePointeeHolder()) ||
                        holder.equals(mPauseButton.getEyePointeeHolder())) {
                    playPauseButtonPointed = true;
                } else if (holder.equals(mForwardButton.getEyePointeeHolder())) {
                    forwardButtonPointed = true;
                } else if (holder.equals(mBackwardButton.getEyePointeeHolder())) {
                    backwardButtonPointed = true;
                } else if (holder.equals(mImaxButton.getEyePointeeHolder())) {
                    imaxButtonPointed = true;
                } else if (holder.equals(mSelectButton.getEyePointeeHolder())) {
                    selectButtonPointed = true;
                } else if (holder.equals(mExitButton.getEyePointeeHolder())) {
                    exitButtonPointed = true;
                }

                //TODO : Need to implement Network dialog display implementation.
                /*else if (holder.equals(mNetworkLayoutSceneObject.getEyePointeeHolder())){
                    networkDialogPointed = true;
                }*/
            }
            if (playPauseButtonPointed || forwardButtonPointed || backwardButtonPointed || imaxButtonPointed
                    || selectButtonPointed || exitButtonPointed || seekbarRatio != null) {
                isAnythingPointed = true;
            }

            handlePlayPauseButton(playPauseButtonPointed, isSingleTapped);
            handleForwardButton(forwardButtonPointed, isSingleTapped);
            handleBackwardButton(backwardButtonPointed, isSingleTapped);
            handleIMaxButton(imaxButtonPointed, isSingleTapped);
            handleSelectedButton(selectButtonPointed, isSingleTapped);
            handleExitButoon(exitButtonPointed, isSingleTapped);
            handleSeekBar(seekbarRatio, isTouched);

            //TODO : Need to implement Network dialog display implementation.
//            handleNetworkDialog(networkDialogPointed, isSingleTapped);

        } else {
            showMediaControllers(false);
            if (isSingleTapped) {
                mIsUIHidden = false;
            }
        }

        handleCursorDisplay();

        if (!isUIHiden && isSingleTapped && !isAnythingPointed) {
            mIsUIHidden = true;
        }
    }

    //TODO : Need to implement Network dialog display implementation.
    private void handleNetworkDialog(boolean networkDialogPointed, boolean isSingleTapped) {
        /*if (networkDialogPointed && isSingleTapped){
            displayNetworkDialog(isSingleTapped);
        }*/
    }

    //TODO : Need to implement Network dialog display implementation.
    private void displayNetworkDialog(boolean isToShow) {
        /*if (isToShow){
            addNetworkDialogToScene();
        }else{
            mainScene.removeSceneObject(mNetworkLayoutSceneObject);
        }*/
    }

    /**
     * Handles cursor display.
     */
    private void handleCursorDisplay() {
        if (!mIsUIHidden) {
            mHeadTracker.getRenderData().setRenderMask(
                    GVRRenderMaskBit.Left | GVRRenderMaskBit.Right);
        } else {
            mHeadTracker.getRenderData().setRenderMask(0);
        }

    }

    /**
     * Handles 'SeekBar' button click and its selector button image..
     *
     * @param seekbarRatio Seek bar ratio
     * @param isTouched whether 'SeekBar' is touched.
     */
    private void handleSeekBar(Float seekbarRatio, boolean isTouched) {
        if (seekbarRatio != null) {
            mSeekbar.glow();
        } else {
            mSeekbar.unglow();
        }

        if (isTouched && seekbarRatio != null) {
            int current = (int) (mMediaPlayer.getDuration() * seekbarRatio);
            mMediaPlayer.seekTo(current);
            mSeekbar.setTime(mGVRContext, current,
                    mMediaPlayer.getDuration());
        } else {
            mSeekbar.setTime(mGVRContext,
                    mMediaPlayer.getCurrentPosition(),
                    mMediaPlayer.getDuration());
        }
    }

    /**
     * Handles 'Exit' button click and its selector button image..
     *
     * @param exitButtonPointed whether cursor is pointed over 'Exit' button
     * @param isSingleTapped whether 'Exit' button is single tapped.
     */
    private void handleExitButoon(boolean exitButtonPointed, boolean isSingleTapped) {
        if (exitButtonPointed) {
            mExitButton.activate();
            if (isSingleTapped) {
                Intent intent = new Intent(mGVRContext.getContext(), VideoListActivity.class);
                mGVRContext.getContext().startActivity(intent);
                mGVRContext.getActivity().finish();
                return;
            }
        }else {
            mExitButton.inactivate();
        }
    }

    /**
     * Handles 'Selected' button click and its selector button image..
     *
     * @param selectButtonPointed whether cursor is pointed over 'Selected' button
     * @param isSingleTapped whether 'Selected' button is single tapped.
     */
    private void handleSelectedButton(boolean selectButtonPointed, boolean isSingleTapped) {
        if (selectButtonPointed) {
            if (isSingleTapped) {
                mFadeWeight = 0.0f;
                mCurrentTheater++;
                if (mCurrentTheater >= NUMBER_OF_THEATER_SCREENS)
                    mCurrentTheater = 0;
            }
            mSelectButton.activate();
        } else {
            mSelectButton.inactivate();
        }
    }

    /**
     * Handles 'iMax' button click and its selector button image..
     *
     * @param imaxButtonPointed whether cursor is pointed over 'iMax' button
     * @param isSingleTapped whether 'iMax' button is single tapped.
     */
    private void handleIMaxButton(boolean imaxButtonPointed, boolean isSingleTapped) {
        if (imaxButtonPointed) {
            if (isSingleTapped) {
                if (!mIsIMAX) {
                    mIsIMAX = true;
                    mTransitionTarget = 2.0f;
                } else {
                    mIsIMAX = false;
                    mTransitionTarget = 1.0f;
                }
            }
            mImaxButton.activate();
        } else {
            mImaxButton.inactivate();
        }
    }

    /**
     * Handles 'backward' button click and its selector button image..
     *
     * @param backwardButtonPointed whether cursor is pointed over 'backward' button
     * @param isSingleTapped whether 'backward' button is single tapped.
     */
    private void handleBackwardButton(boolean backwardButtonPointed, boolean isSingleTapped) {
        if (backwardButtonPointed) {
            if (isSingleTapped) {
                mMediaPlayer
                        .seekTo(mMediaPlayer.getCurrentPosition() - 10000);

            }
            mBackwardButton.activate();
        } else {
            mBackwardButton.inactivate();
        }
    }

    /**
     * Handles 'forward' button click and its selector button image..
     *
     * @param forwardButtonPointed whether cursor is pointed over 'forward' button
     * @param isSingleTapped whether 'forward' button is single tapped.
     */
    private void handleForwardButton(boolean forwardButtonPointed, boolean isSingleTapped) {
        if (forwardButtonPointed) {
            if (isSingleTapped) {
                mMediaPlayer
                        .seekTo(mMediaPlayer.getCurrentPosition() + 10000);
            }
            mForwardButton.activate();
        } else {
            mForwardButton.inactivate();
        }
    }

    /**
     * Handles 'play/pause' button click and its selector button image..
     *
     * @param playPauseButtonPointed whether cursor is pointed over 'play/pause' button
     * @param isSingleTapped whether 'play/pause' button is single tapped.
     */
    private void handlePlayPauseButton(boolean playPauseButtonPointed, boolean isSingleTapped) {
        if (playPauseButtonPointed) {
            if (isSingleTapped) {
                if (mMediaPlayer.isPlaying()) {
                    mMediaPlayer.pause();
                } else {
                    mMediaPlayer.start();
                }
            }
        }

        if (mMediaPlayer.isPlaying()) {
            if (playPauseButtonPointed) {
                mPauseButton.activate();
            } else {
                mPauseButton.inactivate();
            }
        } else {
            if (playPauseButtonPointed) {
                mPlayButton.activate();
            } else {
                mPlayButton.inactivate();
            }
        }
    }

    /**
     * Handles controller display buttons.
     *
     * @param isToShow
     */
    private void showMediaControllers(boolean isToShow) {
        if(isToShow) {
            if (mMediaPlayer.isPlaying()) {
                mPauseButton.show();
                mPlayButton.hide();
            } else {
                mPlayButton.show();
                mPauseButton.hide();
            }
            mForwardButton.show();
            mBackwardButton.show();
            mImaxButton.show();
            mSelectButton.show();
            mExitButton.show();
            mButtonBoard.getRenderData().setRenderMask(
                    GVRRenderMaskBit.Left | GVRRenderMaskBit.Right);
            mSeekbar.setRenderMask(GVRRenderMaskBit.Left | GVRRenderMaskBit.Right);
        }else{
            mPlayButton.hide();
            mPauseButton.hide();
            mForwardButton.hide();
            mBackwardButton.hide();
            mImaxButton.hide();
            mSelectButton.hide();
            mBackwardButton.hide();
            mExitButton.hide();
            mButtonBoard.getRenderData().setRenderMask(0);
            mSeekbar.setRenderMask(0);
            mSeekbar.unglow();
        }
    }

    /**
     * Displays selected Theater screen.
     */
    private void setTheaterScreenScaling() {
        float step = 0.2f;

        mTransitionWeight += step * (mTransitionTarget - mTransitionWeight);
        mFadeWeight += 0.01f * (mFadeTarget - mFadeWeight);

        if (mCurrentTheater == 0) {
            for (int i = 0; i < mTheater[1].getChildrenCount(); i++)
                mTheater[1].getChildByIndex(i).getRenderData()
                        .setRenderMask(0);
            for (int i = 0; i < mTheater[0].getChildrenCount(); i++)
                mTheater[0].getChildByIndex(i).getRenderData().
                        setRenderMask(GVRRenderMaskBit.Left | GVRRenderMaskBit.Right);

            mSceneObject.getRenderData().getMaterial()
                    .setFloat(RadiosityShader.WEIGHT_KEY, mTransitionWeight);
            mSceneObject.getRenderData().getMaterial()
                    .setFloat(RadiosityShader.FADE_KEY, mFadeWeight);
            mSceneObject.getRenderData().getMaterial()
                    .setFloat(RadiosityShader.LIGHT_KEY, 2.0f);
        } else {
            for (int i = 0; i < mTheater[0].getChildrenCount(); i++)
                mTheater[0].getChildByIndex(i).getRenderData()
                        .setRenderMask(0);
            for (int i = 0; i < mTheater[1].getChildrenCount(); i++)
                mTheater[1].getChildByIndex(i).getRenderData()
                        .setRenderMask(GVRRenderMaskBit.Left | GVRRenderMaskBit.Right);

            mOculusSceneObject1.getRenderData().getMaterial()
                    .setFloat(RadiosityShader.WEIGHT_KEY, mTransitionWeight);
            mOculusSceneObject1.getRenderData().getMaterial()
                    .setFloat(RadiosityShader.FADE_KEY, mFadeWeight);
            mOculusSceneObject1.getRenderData().getMaterial()
                    .setFloat(RadiosityShader.LIGHT_KEY, 1.0f);
            mOculusSceneObject2.getRenderData().getMaterial()
                    .setFloat(AdditiveShader.WEIGHT_KEY, mTransitionWeight);
            mOculusSceneObject2.getRenderData().getMaterial()
                    .setFloat(AdditiveShader.FADE_KEY, mFadeWeight);
        }

        float scale = 1.0f + 1.0f * (mTransitionWeight - 1.0f);
        if (scale >= 1.0f) {
            mButtonBoard.getTransform().setScale(scale, scale, 1.0f);
            mButtonBoard.getTransform().setPosition(
                    -0.1f, -0.6f - 0.26f * scale, -8.0f);
            mScreen.getTransform().setScale(scale, scale, 1.0f);
            mSceneObject.getTransform().setScale(scale, scale, 1.0f);
        }
    }

}
