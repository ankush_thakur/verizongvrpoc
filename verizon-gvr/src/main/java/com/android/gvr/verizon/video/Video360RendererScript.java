/* Copyright 2015 Samsung Electronics Co., LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.gvr.verizon.video;

import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.util.Log;

import com.android.gvr.verizon.activity.VideoListActivity;
import com.android.gvr.verizon.view.MediaControllerButton;
import com.android.gvr.verizon.view.livestreamingvideo.ScreenShader;

import org.gearvrf.GVRActivity;
import org.gearvrf.GVRAndroidResource;
import org.gearvrf.GVRExternalTexture;
import org.gearvrf.GVREyePointeeHolder;
import org.gearvrf.GVRMaterial;
import org.gearvrf.GVRMesh;
import org.gearvrf.GVRPicker;
import org.gearvrf.GVRRenderData;
import org.gearvrf.GVRRenderPass;
import org.gearvrf.GVRSceneObject;
import org.gearvrf.GVRTexture;
import org.gearvrf.scene_objects.GVRSphereSceneObject;
import org.gearvrf.scene_objects.GVRVideoSceneObject;

import java.io.IOException;

/**
 *  This class is used to render the Video screen for static(360) videos.
 */
public class Video360RendererScript extends VideoPlayScreenRendererScript
{

    /**
     * TAG.
     */
    private static final String TAG = Video360RendererScript.class.getSimpleName();

    /**
     * Holds the video file name.
     */
    private String mVideoName;

    /**
     * Hold scene objects for MediaControllers.
     */
    private GVRSceneObject mHeadTracker = null;
    private GVRSceneObject mScreen = null;
    private MediaControllerButton mPlayButton = null;
    private MediaControllerButton mPauseButton = null;
    private MediaControllerButton mForwardButton = null;
    private MediaControllerButton mBackwardButton = null;
    private MediaControllerButton mExitButton = null;
//    private VerizonGearSeekbar mSeekbar = null;
//    private GVRSceneObject mButtonBoard = null;
    private MediaControllerButton playButtonSceneObject;
    private MediaControllerButton pauseButtonSceneObject;
    private MediaControllerButton forwardButtonSceneObject;
    private MediaControllerButton backButtonSceneObject;
    private MediaControllerButton exitButtonSceneObject;

    /**
     * Holds boolean whether UI is hidden or displays,depending upon single tap on UI.
     */
    private boolean mIsUIHidden = true;

    /**
     * Holds activity context.
     */
    private GVRActivity mActivity;

    public Video360RendererScript(GVRActivity activity, String videoUrl) {
        mActivity = activity;
        mVideoName = videoUrl;
    }

    @Override
    public void addScreen() {
        GVRExternalTexture screenTexture = new GVRExternalTexture(mGVRContext);
        ScreenShader screenShader = new ScreenShader(mGVRContext);

        GVRSphereSceneObject sphere = new GVRSphereSceneObject(mGVRContext, false);
        GVRMesh screenMesh = sphere.getRenderData().getMesh();

        GVRSceneObject mCinema = new GVRSceneObject(mGVRContext);
        GVRRenderData renderData = new GVRRenderData(mGVRContext);
        GVRMaterial material = new GVRMaterial(mGVRContext,
                screenShader.getShaderId());
        material.setTexture(ScreenShader.SCREEN_KEY, screenTexture);
        renderData.setMesh(screenMesh);
        renderData.setMaterial(material);

        mScreen = new GVRVideoSceneObject(mGVRContext, screenMesh, mMediaPlayer,
                screenTexture, GVRVideoSceneObject.GVRVideoType.MONO);
        mScreen.attachRenderData(renderData);
        mScreen.getRenderData().setCullFace(GVRRenderPass.GVRCullFaceEnum.None);

        mCinema.addChildObject(mScreen);
        mainScene.addSceneObject(mCinema);
    }

    @Override
    public void addCursor() {

        GVRTexture headTrackerTexture = null;
        try {
            headTrackerTexture = mGVRContext
                    .loadTexture(new GVRAndroidResource(mGVRContext,
                            "head-tracker.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        mHeadTracker = new GVRSceneObject(mGVRContext,
                mGVRContext.createQuad(0.5f, 0.5f), headTrackerTexture);
        mHeadTracker.getTransform().setPositionZ(-9.0f);
        mHeadTracker.getRenderData().setRenderingOrder(
                GVRRenderData.GVRRenderingOrder.OVERLAY);
        mHeadTracker.getRenderData().setDepthTest(false);
        mHeadTracker.getRenderData().setRenderingOrder(100000);
        mainScene.getMainCameraRig().addChildObject(mHeadTracker);
    }

    @Override
    public void addMediaControllerButtons() {

        /**
         * Play button
         */
        GVRTexture mInactivePlay = null;
        try {
            mInactivePlay = mGVRContext.loadTexture(new GVRAndroidResource(
                    mGVRContext, "button/play-inactive.png"));
            GVRTexture mActivePlay = mGVRContext.loadTexture(new GVRAndroidResource(
                    mGVRContext, "button/play-active.png"));
            mPlayButton = new MediaControllerButton(mGVRContext, mGVRContext.createQuad(0.7f, 0.7f),
                    mActivePlay, mInactivePlay);
            playButtonSceneObject = new MediaControllerButton(mGVRContext,
                    mGVRContext.createQuad(0.9f, 0.9f), mActivePlay, mInactivePlay);
            playButtonSceneObject.setPosition(0.0f, -3.8f, -10.0f);;
            playButtonSceneObject.getRenderData().setRenderingOrder(
                    GVRRenderData.GVRRenderingOrder.TRANSPARENT);
            playButtonSceneObject.getRenderData().setDepthTest(false);
            playButtonSceneObject.addChildObject(mPlayButton);
            mainScene.addSceneObject(playButtonSceneObject);

            /**
             * Pause button
             */
            GVRTexture mInactivePause = mGVRContext.loadTexture(new GVRAndroidResource(
                    mGVRContext, "button/pause-inactive.png"));
            GVRTexture mActivePause = mGVRContext.loadTexture(new GVRAndroidResource(
                    mGVRContext, "button/pause-active.png"));
            mPauseButton = new MediaControllerButton(mGVRContext, mGVRContext.createQuad(0.7f, 0.7f),
                    mActivePause, mInactivePause);
            pauseButtonSceneObject = new MediaControllerButton(mGVRContext,
                    mGVRContext.createQuad(0.9f, 0.9f), mActivePause, mInactivePause);
            pauseButtonSceneObject.setPosition(0.0f, -3.8f, -10.0f);
            pauseButtonSceneObject.getRenderData().setRenderingOrder(
                    GVRRenderData.GVRRenderingOrder.TRANSPARENT);
            pauseButtonSceneObject.getRenderData().setDepthTest(false);
            pauseButtonSceneObject.addChildObject(mPauseButton);
            mainScene.addSceneObject(pauseButtonSceneObject);

            /**
             * FORWARD button
             */
            GVRTexture mInactiveFront = mGVRContext.loadTexture(new GVRAndroidResource(
                    mGVRContext, "button/front-inactive.png"));
            GVRTexture mActiveFront = mGVRContext.loadTexture(new GVRAndroidResource(
                    mGVRContext, "button/front-active.png"));
            mForwardButton = new MediaControllerButton(mGVRContext, mGVRContext.createQuad(0.7f, 0.7f),
                    mActiveFront, mInactiveFront);
            forwardButtonSceneObject = new MediaControllerButton(mGVRContext,
                    mGVRContext.createQuad(0.9f, 0.9f), mActiveFront, mInactiveFront);
            forwardButtonSceneObject.setPosition(1.2f, -3.8f, -10.0f);
            forwardButtonSceneObject.getRenderData().setRenderingOrder(
                    GVRRenderData.GVRRenderingOrder.TRANSPARENT);
            forwardButtonSceneObject.getRenderData().setDepthTest(false);
            forwardButtonSceneObject.addChildObject(mForwardButton);
            mainScene.addSceneObject(forwardButtonSceneObject);

            /*
            * BACKWARD button
            */
            GVRTexture mInactiveBack = mGVRContext.loadTexture(new GVRAndroidResource(
                    mGVRContext, "button/back-inactive.png"));
            GVRTexture mActiveBack = mGVRContext.loadTexture(new GVRAndroidResource(
                    mGVRContext, "button/back-active.png"));
            mBackwardButton = new MediaControllerButton(mGVRContext, mGVRContext.createQuad(0.7f, 0.7f),
                    mActiveBack, mInactiveBack);
            backButtonSceneObject = new MediaControllerButton(mGVRContext,
                    mGVRContext.createQuad(0.9f, 0.9f), mActiveBack, mInactiveBack);
            backButtonSceneObject.setPosition(-1.2f, -3.8f, -10.0f);
            backButtonSceneObject.getRenderData().setRenderingOrder(
                    GVRRenderData.GVRRenderingOrder.TRANSPARENT);
            backButtonSceneObject.getRenderData().setDepthTest(false);
            backButtonSceneObject.addChildObject(mBackwardButton);
            mainScene.addSceneObject(backButtonSceneObject);

             /*
             * Exit button
             */
            GVRTexture mInactiveExit = mGVRContext.loadTexture(new GVRAndroidResource(
                    mGVRContext, "button/exit_unpressed.png"));
            GVRTexture mActiveExit = mGVRContext.loadTexture(new GVRAndroidResource(
                    mGVRContext, "button/exit_pressed.png"));
            mExitButton = new MediaControllerButton(mGVRContext, mGVRContext.createQuad(1.5f, 0.25f),
                    mActiveExit, mInactiveExit);
            exitButtonSceneObject = new MediaControllerButton(mGVRContext,
                    mGVRContext.createQuad(1.5f, 0.9f), mActiveExit, mInactiveExit);
            exitButtonSceneObject.setPosition(2.9f, -3.8f, -10.0f);
            exitButtonSceneObject.getRenderData().setRenderingOrder(
                    GVRRenderData.GVRRenderingOrder.TRANSPARENT);
            exitButtonSceneObject.getRenderData().setDepthTest(false);
            exitButtonSceneObject.addChildObject(mExitButton);
            mainScene.addSceneObject(exitButtonSceneObject);

            showMediaControllers(false);

        } catch (IOException e) {
            mActivity.finish();
            e.printStackTrace();
        }
    }

    @Override
    public void addSeekBar() {

    }

    @Override
    public void playVideo() {
        AssetFileDescriptor afd = null;
        try {
            afd = mGVRContext.getContext().getAssets().openFd(mVideoName);
            mMediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            afd.close();

            mMediaPlayer.prepare();
            mMediaPlayer.setLooping( true );
            mMediaPlayer.start();
        } catch (IOException e) {
            e.printStackTrace();
            mActivity.finish();
            Log.e(TAG, "Assets(video) were not loaded in playVideo(). Stopping application!");
        }

    }

    @Override
    public void onStep() {
        float step = 0.2f;

        boolean isTouched = mIsTouched;
        boolean isSingleTapped = mIsSingleTapped;
        mIsTouched = false;
        mIsSingleTapped = false;

        boolean isUIHiden = mIsUIHidden;
        boolean isAnythingPointed = false;

        if (!mIsUIHidden) {

            showMediaControllers(true);

            // Detect scene objects(MediaController buttons) here.
            GVREyePointeeHolder[] pickedHolders = GVRPicker.pickScene(mGVRContext.getMainScene());
            boolean playPauseButtonPointed = false;
            boolean forwardButtonPointed = false;
            boolean backwardButtonPointed = false;
            boolean exitButtonPointed = false;
//            Float seekbarRatio = mSeekbar.getRatio(mGVRContext.getMainScene()
//                    .getMainCameraRig().getLookAt());
            for (GVREyePointeeHolder holder : pickedHolders) {
                if (holder.equals(playButtonSceneObject.getEyePointeeHolder()) ||
                        holder.equals(pauseButtonSceneObject.getEyePointeeHolder())) {
                    playPauseButtonPointed = true;
                } else if (holder.equals(forwardButtonSceneObject.getEyePointeeHolder())) {
                    forwardButtonPointed = true;
                } else if (holder.equals(backButtonSceneObject.getEyePointeeHolder())) {
                    backwardButtonPointed = true;
                } else if (holder.equals(mExitButton.getEyePointeeHolder())) {
                    exitButtonPointed = true;
                }
            }
            if (playPauseButtonPointed || forwardButtonPointed || backwardButtonPointed || exitButtonPointed /*|| seekbarRatio != null*/) {
                isAnythingPointed = true;
            }

            handlePlayPauseButton(playPauseButtonPointed, isSingleTapped);
            handleForwardButton(forwardButtonPointed, isSingleTapped);
            handleBackwardButton(backwardButtonPointed, isSingleTapped);
            handleExitButoon(exitButtonPointed, isSingleTapped);

//            if (seekbarRatio != null) {
//                mSeekbar.glow();
//            } else {
//                mSeekbar.unglow();
//            }
//
//            if (isTouched && seekbarRatio != null) {
//                int current = (int) (mMediaPlayer.getDuration() * seekbarRatio);
//                mMediaPlayer.seekTo(current);
//                mSeekbar.setTime(mGVRContext, current,
//                        mMediaPlayer.getDuration());
//            } else {
//                mSeekbar.setTime(mGVRContext,
//                        mMediaPlayer.getCurrentPosition(),
//                        mMediaPlayer.getDuration());
//            }
        } else {
            showMediaControllers(false);

            if (isSingleTapped) {
                mIsUIHidden = false;
            }
        }

        handleCursorDisplay();

        if (!isUIHiden && isSingleTapped && !isAnythingPointed) {
            mIsUIHidden = true;
        }
    }

    /**
     * Handles cursor display.
     */
    private void handleCursorDisplay() {
        if (!mIsUIHidden) {
            mHeadTracker.getRenderData().setRenderMask(
                    GVRRenderData.GVRRenderMaskBit.Left | GVRRenderData.GVRRenderMaskBit.Right);
        } else {
            mHeadTracker.getRenderData().setRenderMask(0);
        }
    }

    private void handleExitButoon(boolean exitButtonPointed, boolean isSingleTapped) {
        if (exitButtonPointed) {
            exitButtonSceneObject.activate();
            if (isSingleTapped) {
                mGVRContext.getActivity().finish();
                Intent intent = new Intent(mGVRContext.getContext(), VideoListActivity.class);
                mGVRContext.getContext().startActivity(intent);
                return;
            }
        }else {
            exitButtonSceneObject.inactivate();
        }
    }

    /**
     * Handles 'backward' button click and its selector button image..
     *
     * @param backwardButtonPointed whether cursor is pointed over 'backward' button
     * @param isSingleTapped whether 'backward' button is single tapped.
     */
    private void handleBackwardButton(boolean backwardButtonPointed, boolean isSingleTapped) {
        if (backwardButtonPointed) {
            if (isSingleTapped) {
                mMediaPlayer
                        .seekTo(mMediaPlayer.getCurrentPosition() - 10000);

            }
            backButtonSceneObject.activate();
        } else {
            backButtonSceneObject.inactivate();
        }
    }

    /**
     * Handles 'forward' button click and its selector button image..
     *
     * @param forwardButtonPointed whether cursor is pointed over 'forward' button
     * @param isSingleTapped whether 'forward' button is single tapped.
     */
    private void handleForwardButton(boolean forwardButtonPointed, boolean isSingleTapped) {
        if (forwardButtonPointed) {
            if (isSingleTapped) {
                mMediaPlayer
                        .seekTo(mMediaPlayer.getCurrentPosition() + 10000);
            }
            forwardButtonSceneObject.activate();
        } else {
            forwardButtonSceneObject.inactivate();
        }
    }

    /**
     * Handles 'play/pause' button click and its selector button image..
     *
     * @param playPauseButtonPointed whether cursor is pointed over 'play/pause' button
     * @param isSingleTapped whether 'play/pause' button is single tapped.
     */
    private void handlePlayPauseButton(boolean playPauseButtonPointed, boolean isSingleTapped) {
        if (playPauseButtonPointed) {
            if (isSingleTapped) {
                if (mMediaPlayer.isPlaying()) {
                    mMediaPlayer.pause();
                } else {
                    mMediaPlayer.start();
                }
            }
        }

        if (mMediaPlayer.isPlaying()) {
            if (playPauseButtonPointed) {
                pauseButtonSceneObject.activate();
            } else {
                pauseButtonSceneObject.inactivate();
            }
        } else {
            if (playPauseButtonPointed) {
                playButtonSceneObject.activate();
            } else {
                playButtonSceneObject.inactivate();
            }
        }
    }

    private void showMediaControllers(boolean isToShow) {
        if (isToShow) {
            if (mMediaPlayer.isPlaying()) {
                pauseButtonSceneObject.show();
                playButtonSceneObject.hide();
            } else {
                playButtonSceneObject.show();
                pauseButtonSceneObject.hide();
            }
            forwardButtonSceneObject.show();
            backButtonSceneObject.show();
            exitButtonSceneObject.show();
//            mButtonBoard.getRenderData().setRenderMask(
//                    GVRRenderData.GVRRenderMaskBit.Left | GVRRenderData.GVRRenderMaskBit.Right);
//            mSeekbar.setRenderMask(GVRRenderData.GVRRenderMaskBit.Left | GVRRenderData.GVRRenderMaskBit.Right);
        }else{
            playButtonSceneObject.hide();
            pauseButtonSceneObject.hide();
            forwardButtonSceneObject.hide();
            backButtonSceneObject.hide();
            exitButtonSceneObject.hide();
//        mButtonBoard.getRenderData().setRenderMask(0);
//        mSeekbar.setRenderMask(0);
//        mSeekbar.unglow();
        }
    }

}
