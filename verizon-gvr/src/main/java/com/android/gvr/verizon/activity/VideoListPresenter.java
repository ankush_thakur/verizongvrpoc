package com.android.gvr.verizon.activity;

import android.content.Context;
import android.view.View;

import com.android.gvr.verizon.model.VideoContents;
import com.android.gvr.verizon.util.Constants;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * This <b>Presenter</b>, helps updating the <b>Model</b>(VideoContents) and interact with the <b>View</b>(VideoListActivity) to update the video list.
 * The responsibilities of this presenter are:
 * <ul>
 * <li>Listens to user action
 * <li>Updates <b>Model</b> and <b>View</b>(VideoListActivity)
 * </ul>
 */
public class VideoListPresenter {

    /**
     * Holds IVideoListView object.
     */
    private IVideoListView mView;

    /**
     * Holds Context.
     */
    private Context mContext;

    public VideoListPresenter(Context context, IVideoListView view){
        mContext = context;
        mView = view;
    }

    /**
     * Load the Video contents.(Later the content will be replace with web content)
     */
    public ArrayList<VideoContents> getVideoContents() {
        String videoJson = getVideoJson();
        Gson gson = new Gson();
        VideoContents videoContent = gson.fromJson(videoJson, VideoContents.class);
        ArrayList<VideoContents> mVideosContentList = videoContent.getVideosContentList();
        return mVideosContentList;
    }

    /**
     * Reads the videos.json file from assets.
     *
     * @return json string of videos content.
     */
    private String getVideoJson() {
        String videosJson = null;
        try {
            InputStream inputStream = mContext.getAssets().open(Constants.VIDEO_JSON_FILE);
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            videosJson = new String(buffer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return videosJson;
    }

    public void updateVideoListItem(ArrayList<VideoContents> videoContentList){
        mView.updateVideoList(videoContentList);
    }

    public void displayError(boolean isToShow) {
        mView.displayError(isToShow);
    }

    public void displayProgress(boolean isToShow) {
        mView.displayProgress(isToShow);
    }

    public void displayVideoList(boolean isToShow) {
        mView.displayVideoList(isToShow);
    }

    public void displayNetworkDialog(boolean isToShow) {
        mView.displayNetworkDialog(isToShow);
    }

    /**
     * This is View interface to update the activity(View)
     */
    public interface IVideoListView {
        /**
         * Updates the video list received after videos.json parsing.
         *
         * @param videoContentList video list to update.
         */
        void updateVideoList(ArrayList<VideoContents> videoContentList);

        /**
         * Displays Video list.
         *
         * @param isToShow whether to show video list.
         */
        void displayVideoList(boolean isToShow);

        /**
         * Displays error text status.
         *
         * @param isToShow whether to show error text.
         */
        void displayError(boolean isToShow);

        /**
         * Displays progress bar.
         *
         * @param isToShow whether to show progress bar.
         */
        void displayProgress(boolean isToShow);

        /**
         * Displays Network dialog.
         *
         * @param isToShow whether to network dialog.
         */
        void displayNetworkDialog(boolean isToShow);

    }
}
