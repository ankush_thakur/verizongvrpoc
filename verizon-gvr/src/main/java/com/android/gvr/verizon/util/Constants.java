package com.android.gvr.verizon.util;

/**
 * Created by subeerkumar.verma on 05-07-2016.
 */
public class Constants {

    /**
     * Holds string constants for Intent key.
     */
    public static final String KEY_IS_STATIC_VIDEO = "key_is_static_video";
    public static final String KEY_VIDEO_URL = "key_video_url";

    /**
     * Holds the json array name in videos.json file.
     */
    public static final String VIDEO_JSON_FILE = "videos.json";

}
