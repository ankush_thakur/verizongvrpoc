package com.android.gvr.verizon.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.gvr.verizon.R;
import com.android.gvr.verizon.model.VideoContents;

import java.util.ArrayList;

/**
 * Video grid adapter for binding video contents to gridview..
 * <p/>
 * Created by ankush.thakur on 6/21/2016.
 */
public class VideoGridAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<VideoContents> mVideoContentList = new ArrayList<VideoContents>();

    public VideoGridAdapter(Context c, ArrayList<VideoContents> videoContents) {
        mContext = c;
        mVideoContentList = videoContents;
    }

    @Override
    public int getCount() {
        return mVideoContentList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View grid = convertView;
        ViewHolder holder = null;
        if (mVideoContentList.size() > 0) {
            if (grid == null) {
                LayoutInflater inflater = (LayoutInflater) mContext
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                grid = inflater.inflate(R.layout.grid_item, null);
                holder = new ViewHolder();
                holder.imageTitle = (TextView) grid.findViewById(R.id.grid_text);
                holder.image = (ImageView) grid.findViewById(R.id.grid_image);
                holder.duration = (TextView) grid.findViewById(R.id.duration);
                grid.setTag(holder);

                TextView textView = (TextView) grid.findViewById(R.id.grid_text);
                textView.setSelected(true);
                ImageView imageView = (ImageView) grid.findViewById(R.id.grid_image);
                TextView duration = (TextView) grid.findViewById(R.id.duration);
                textView.setText(mVideoContentList.get(position).getVideoName());
                imageView.setImageDrawable(mVideoContentList.get(position).getVideoImage(mContext));
                duration.setText(mVideoContentList.get(position).getVideoDuration(mContext));
            } else {
                holder = (ViewHolder) grid.getTag();
            }

            holder.imageTitle.setText(mVideoContentList.get(position).getVideoName());
            holder.imageTitle.setSelected(true);
            holder.image.setImageDrawable(mVideoContentList.get(position).getVideoImage(mContext));
            holder.duration.setText(mVideoContentList.get(position).getVideoDuration(mContext));
        }
        return grid;
    }

    class ViewHolder {
        TextView imageTitle;
        ImageView image;
        TextView duration;
    }
}