package com.android.gvr.verizon.util;

import android.content.Context;
import android.content.Intent;

import com.android.gvr.verizon.service.MusicService;

/**
 * Util class responsible for MusicService.
 */
public class MusicServiceUtil {

    private static Intent sMusicServiceIntent;

    /**
     * Starts MusicService.
     *
     * @param context Context
     */
    public static void startMusic(Context context){
        sMusicServiceIntent = new Intent(context, MusicService.class);
        context.startService(sMusicServiceIntent);
    }

    /**
     * Stops MusicService.
     *
     * @param context Context
     */
    public static void stopMusic(Context context){
        if (sMusicServiceIntent != null) {
            context.stopService(sMusicServiceIntent);
        }
    }
}
