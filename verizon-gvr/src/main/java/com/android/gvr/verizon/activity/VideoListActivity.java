package com.android.gvr.verizon.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.gvr.verizon.R;
import com.android.gvr.verizon.adapter.VideoGridAdapter;
import com.android.gvr.verizon.model.VideoContents;
import com.android.gvr.verizon.util.Constants;
import com.android.gvr.verizon.util.MusicServiceUtil;
import com.android.gvr.verizon.util.NetworkUtil;
import com.android.gvr.verizon.video.VideoListRendererScript;

import org.gearvrf.GVRActivity;
import org.gearvrf.GVRMain;
import org.gearvrf.scene_objects.view.GVRFrameLayout;

import java.util.ArrayList;

/**
 * Screen which shows video list.
 *
 * @author ankush.thakur
 */
public class VideoListActivity extends GVRActivity implements VideoListPresenter.IVideoListView {

    /**
     * Tag for logging.
     */
    private static final String TAG = VideoListActivity.class.getSimpleName();

    /**
     * Holds the context.
     */
    private Context mContext;

    /**
     * Holds Video list parent layout.
     */
    private LinearLayout mVideoListParentLayout;

    /**
     * Holds Progress bar.
     */
    private ProgressBar mProgressBar;

    /**
     * GVR main to add the content on screen.
     */
    private GVRMain mGVRMain;

    /**
     * GVR frame layout to add an Android frame layout on top of it.
     */
    private GVRFrameLayout mGVRFrameLayout;

    /**
     * Android Grid view to show the list of Videos.
     */
    private GridView mVideoGridView;

    /**
     * Heading text view
     */
    private TextView mHeadingText;

    /**
     * Holds Error text view.
     */
    private TextView mErrorTextView;

    /**
     * Holds VideoContentTask object.
     */
    private LoadVideoContentTask mLoadVideoContentTask;

    /**
     * Arraylist to hold the video contents.
     */
    private ArrayList<VideoContents> mVideosContentList;

    /**
     * Holds VideoListPresenter object;
     */
    private VideoListPresenter mVideoListPresenter;

    /**
     * Holds network parent layout.
     */
    private LinearLayout mNetworkLayout;

    /**
     * Holds Network dialog OK button.
     */
    private Button mNetworkDialogOKButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
        mVideoListPresenter.displayVideoList(false);
        mVideoListPresenter.displayError(false);
        mVideoListPresenter.displayProgress(false);
        mVideoListPresenter.displayNetworkDialog(false);

        startLoadVideoContentTask();

        setMain(mGVRMain, "gvr_video_list_screen.xml");
    }

    @Override
    protected void onResume() {
        MusicServiceUtil.startMusic(this);
        super.onResume();
    }

    @Override
    protected void onPause() {
        MusicServiceUtil.stopMusic(this);
        super.onPause();
    }

    @Override
    protected void onStop() {
        MusicServiceUtil.stopMusic(this);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        if (mLoadVideoContentTask != null){
            mLoadVideoContentTask.cancel(true);
        }

        MusicServiceUtil.stopMusic(this);
        super.onDestroy();
    }

    /**
     * Initializes data members.
     */
    private void init() {
        mContext = this;
        mGVRFrameLayout = new GVRFrameLayout(this);
        mGVRFrameLayout.setAlpha(0);
        mGVRFrameLayout.setBackgroundColor(Color.parseColor("#00000000"));
        View.inflate(this, R.layout.activity_main, mGVRFrameLayout);

        mVideoListParentLayout = (LinearLayout) mGVRFrameLayout.findViewById(R.id.listLayout);
        mVideoGridView = (GridView) findViewById(R.id.gridview);
        mProgressBar = (ProgressBar) mGVRFrameLayout.findViewById(R.id.progress);
        mHeadingText = (TextView) mGVRFrameLayout.findViewById(R.id.heading);
        mErrorTextView = (TextView) mGVRFrameLayout.findViewById(R.id.error_textview);
        mNetworkLayout = (LinearLayout) mGVRFrameLayout.findViewById(R.id.network_layout);
        mNetworkDialogOKButton = (Button) mGVRFrameLayout.findViewById(R.id.ok_button_videolist_network_dialog);
        Typeface tf = Typeface.createFromAsset(getAssets(),
                "fonts/altehaasgroteskbold.ttf");
        mHeadingText.setTypeface(tf);
        mVideoListPresenter = new VideoListPresenter(this, this);
        mVideoGridView.setOnItemClickListener(mVideoGridItemClickListener);
        mNetworkDialogOKButton.setOnClickListener(mNetworkDialogOKButtonClickListener);
        mGVRMain = new VideoListRendererScript(this, mGVRFrameLayout);
    }

    /**
     * Starts LoadVideoContentTask.
     */
    private void startLoadVideoContentTask() {
        if (mLoadVideoContentTask != null){
            mLoadVideoContentTask.cancel(true);
        }

        mLoadVideoContentTask = new LoadVideoContentTask();
        mLoadVideoContentTask.execute();
    }

    /**
     * Asynctask to load the video contents from videos.json file.
     */
    private class LoadVideoContentTask extends AsyncTask<Void, Void, Void>{

        @Override
        protected void onPreExecute() {
            mVideoListPresenter.displayError(false);
            mVideoListPresenter.displayProgress(true);
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            mVideosContentList = mVideoListPresenter.getVideoContents();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            mVideoListPresenter.displayProgress(false);
            mVideoListPresenter.updateVideoListItem(mVideosContentList);
            super.onPostExecute(aVoid);
        }
    }

    /**
     * Click listener for Video grid items.
     */
    private OnItemClickListener mVideoGridItemClickListener = new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                long arg3) {
            boolean isStaticVideo = mVideosContentList.get(position).isStaticVideo();
            MusicServiceUtil.stopMusic(VideoListActivity.this);
            if (!isStaticVideo && !NetworkUtil.isNetworkAvailable(mContext)){
                mVideoListPresenter.displayNetworkDialog(true);
                return;
            }

            launchVideoPlayScreen(position, isStaticVideo);
        }
    };

    private View.OnClickListener mNetworkDialogOKButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mVideoListPresenter.displayNetworkDialog(false);
        }
    };

    /**
     * Launches VideoPlayScreen
     *
     * @param itemPosition the item position in Grid which is clicked.
     */
    private void launchVideoPlayScreen(int itemPosition, boolean isStaticVideo) {
        Intent videoPlayScreenIntent = new Intent(mContext, VideoPlayActivity.class);
        String videoUrl = mVideosContentList.get(itemPosition).getUrl();
        videoPlayScreenIntent.putExtra(Constants.KEY_IS_STATIC_VIDEO, isStaticVideo);
        videoPlayScreenIntent.putExtra(Constants.KEY_VIDEO_URL, videoUrl);
        startActivity(videoPlayScreenIntent);
        getGVRContext().getActivity().finish();
    }


    @Override
    public void updateVideoList(ArrayList<VideoContents> videoContentList) {
        if (mVideosContentList != null && mVideosContentList.size() > 0) {
            mVideoListPresenter.displayVideoList(true);
            VideoGridAdapter adapter = new VideoGridAdapter(VideoListActivity.this, mVideosContentList);
            mVideoGridView.setAdapter(adapter);
        }else{
            mVideoListPresenter.displayVideoList(false);
            mVideoListPresenter.displayError(true);
        }
    }

    @Override
    public void displayError(boolean isToShow) {
        if (isToShow) {
            mErrorTextView.setVisibility(View.VISIBLE);
        }else{
            mErrorTextView.setVisibility(View.GONE);
        }
    }

    @Override
    public void displayProgress(boolean isToShow) {
        if (isToShow) {
            mProgressBar.setVisibility(View.VISIBLE);
        }else{
            mProgressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void displayVideoList(boolean isToShow) {
        if (isToShow) {
            mVideoListParentLayout.setVisibility(View.VISIBLE);
        }else{
            mVideoListParentLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void displayNetworkDialog(boolean isToShow) {
        if (isToShow) {
            mNetworkLayout.setVisibility(View.VISIBLE);
        }else{
            mNetworkLayout.setVisibility(View.GONE);
        }
    }

}