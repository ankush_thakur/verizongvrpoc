/* Copyright 2015 Samsung Electronics Co., LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.gvr.verizon.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;

import com.android.gvr.verizon.util.Constants;
import com.android.gvr.verizon.util.VRTouchPadGestureDetector;
import com.android.gvr.verizon.video.VideoPlayScreenRendererScript;
import com.android.gvr.verizon.video.factory.GVRMainFactory;

import org.gearvrf.GVRActivity;

/**
 * This activity is responsible for playing static(360)/live streaming videos.
 */
public class VideoPlayActivity extends GVRActivity implements VRTouchPadGestureDetector.OnTouchPadGestureListener
{
    /**
     * Holds GestureDetector object.
     */
    private VRTouchPadGestureDetector mDetector = null;

    /**
     * Holds the video url('video file name' for static videos & 'url' for live streaming videos.)
     */
    private String mVideoUrl;

    /**
     * Holds boolean whether video is static(360) or live streaming.
     */
    private boolean mIsStaticVideo;

    /**
     * Holds GVRMain object as oer the video type(static/live streaming.)
     */
    private VideoPlayScreenRendererScript mGvrMain;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        if (intent != null) {
            mIsStaticVideo = intent.getBooleanExtra(Constants.KEY_IS_STATIC_VIDEO, false);
            mVideoUrl = intent.getStringExtra(Constants.KEY_VIDEO_URL);
        }

        mGvrMain = GVRMainFactory.getGVRMain(this, mIsStaticVideo, mVideoUrl);
        setMain(mGvrMain, "gvr_video_screen.xml");

        mDetector = new VRTouchPadGestureDetector(this);
    }

    @Override
    protected void onDestroy() {
        mGvrMain.cleanUpResources();
        mGvrMain = null;
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        mGvrMain.onPause();
        super.onPause();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mDetector.onTouchEvent(event);
        mGvrMain.onTouchEvent();
        return super.onTouchEvent(event);
    }

    @Override
    public void onBackPressed() {
        getGVRContext().getActivity().finish();
        super.onBackPressed();
    }

    @Override
    public boolean onSingleTap(MotionEvent e) {
        mGvrMain.onSingleTap();
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {
        Log.v("", "onLongPress");
    }

    @Override
    public boolean onSwipe(MotionEvent e, VRTouchPadGestureDetector.SwipeDirection swipeDirection, float velocityX, float velocityY) {
        Log.v("", "onSwipe");
        return false;
    }
}
